package org.example.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserInputServiceTest {

	private CustomScanner customScanner = mock(CustomScanner.class);

	private UserInputService userInputService = new UserInputService(customScanner);

	@Test
	public void addingTaskCommandShouldReturnCorrectEnumValue(){
		//given
		final String addTaskCmd = "add-task";

		//when
		when(customScanner.readInputString()).thenReturn(addTaskCmd);
		final Options command = userInputService.getCommand();

		//then
		assertEquals(Options.ADD, command);
	}
}