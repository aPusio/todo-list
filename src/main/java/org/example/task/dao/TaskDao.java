package org.example.task.dao;

import java.util.List;
import java.util.Optional;

import org.example.HibernateFactory;
import org.example.task.models.Task;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class TaskDao {

    public Task save(Task task) {
        final SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();
        final Long savedId = (Long) session.save(task);
        transaction.commit();
        final Task taskFromDb = session.get(Task.class, savedId);
        session.close();
        return taskFromDb;
    }

    public Task update(Task task) {
        final SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();
        session.update(task);
        transaction.commit();
        final Task taskFromDb = session.get(Task.class, task.getId());
        session.close();
        return taskFromDb;
    }

    public Task remove(Long id) {
        final SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();
        final Task task = session.get(Task.class, id);
        session.remove(task);
        transaction.commit();
        session.close();
        return task;
    }

    public Optional<Task> get(Long id) {
        final SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
        final Session session = sessionFactory.openSession();
        final Task taskFromDb = session.get(Task.class, id);
        session.close();
        if (taskFromDb != null) return Optional.of(taskFromDb);

        return Optional.empty();
    }

    public List<Task> getAll() {
        final SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
        final Session session = sessionFactory.openSession();
        final Query<Task> getAllTasksQuery = session.createQuery("from Task", Task.class);
        final List<Task> tasksFromDb = getAllTasksQuery.getResultList();
        session.close();
        return tasksFromDb;
    }
}