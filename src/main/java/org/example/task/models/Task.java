package org.example.task.models;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String taskName;
    private boolean finished;

    public Task(String taskName, boolean finished) {
        this.taskName = taskName;
        this.finished = finished;
    }
}
