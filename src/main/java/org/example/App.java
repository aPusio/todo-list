package org.example;

import java.util.Scanner;

import org.example.cli.CustomScanner;
import org.example.cli.UserInputService;
import org.example.task.dao.TaskDao;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        final CustomScanner customScanner = new CustomScanner(new Scanner(System.in));
        final UserInputService userInputService = new UserInputService(customScanner);
        final TaskDao taskDao = new TaskDao();
        final AppEngine appEngine = new AppEngine(userInputService, taskDao);

        appEngine.init();
    }
}
