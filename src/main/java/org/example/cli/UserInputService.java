package org.example.cli;

import org.example.task.models.Task;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserInputService {
	private final CustomScanner userInterface;

	public Options getCommand() {
		System.out.println(MessagesService.initMessage());
		Options userCmd;
		do {
			userCmd = Options.parseInput(userInterface.readInputString());
		} while (userCmd == Options.UNKNOWN);
		return userCmd;
	}

	public Task readTask() {
		return new Task(readTaskName(), readTaskState());
	}

	private Boolean readTaskState() {
		System.out.println(MessagesService.TYPE_STATE_NAME);
		return userInterface.readInputBoolean();
	}

	private String readTaskName() {
		System.out.println(MessagesService.TYPE_TASK_NAME);
		return userInterface.readInputString();
	}
}
