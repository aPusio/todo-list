package org.example.cli;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.example.task.models.Task;

public class MessagesService {
	private static final String WELCOME_MESSAGE = "PLEASE TYPE ONE OPTION: \r\n";
	public static final String TYPE_TASK_NAME = "PLEASE TYPE NAME OF THE TASK: ";
	public static final String TYPE_STATE_NAME = "PLEASE TYPE TASK STATE (true/false): ";
	private static final int MAX_TASK_LENGTH = 25;

	public static String initMessage() {
		return WELCOME_MESSAGE +
				   Arrays.stream(Options.values())
					   .map(Options::getCommand)
					   .collect(Collectors.joining("\r\n"));
	}

	public static String toCmdMessage(Task task) {
		return StringUtils.rightPad(task.getTaskName(), MAX_TASK_LENGTH) +
				   ", " +
				   (task.isFinished() ? "DONE" : "TODO");
	}
}
