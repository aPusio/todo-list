package org.example.cli;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CustomScanner {

	private final Scanner scanner;

	public String readInputString() {
		try {
			return scanner.nextLine();
		} catch (NoSuchElementException e) {
			return StringUtils.EMPTY;
		}
	}

	public boolean readInputBoolean() {
		return Boolean.parseBoolean(scanner.nextLine());
	}
}
