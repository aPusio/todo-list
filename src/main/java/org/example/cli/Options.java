package org.example.cli;

import java.util.Arrays;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Options {
	ADD("add-task"),
	REMOVE("remove-task"),
	PRINT_ALL("print-all-tasks"),
	QUIT("quit"),
	UNKNOWN("");

	private final String command;

	public static Options parseInput(String input){
		return Arrays.stream(Options.values())
			.filter(command -> command.getCommand().equals(input))
				   .findFirst()
				   .orElse(Options.UNKNOWN);
	}
}
