package org.example;

import java.util.stream.Collectors;

import org.example.cli.MessagesService;
import org.example.cli.Options;
import org.example.cli.UserInputService;
import org.example.task.dao.TaskDao;
import org.example.task.models.Task;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AppEngine {
	private final UserInputService userInputService;
	private final TaskDao taskDao;

	public void init() {
		Options userCommand = Options.UNKNOWN;
		while (userCommand != Options.QUIT) {
			userCommand = userInputService.getCommand();
			switch (userCommand) {
				case ADD:
					final Task task = userInputService.readTask();
					taskDao.save(task);
					break;
				case PRINT_ALL:
					System.out.println(taskDao.getAll()
						.stream()
						.map(MessagesService::toCmdMessage)
						.collect(Collectors.joining("\r\n")));
					break;
			}
		}
	}
}
